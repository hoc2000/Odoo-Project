{
    "name": "Clarity Backend Theme for community",
    "version": "16.0.1.0.0",
    "author": "Terabits Technolab",
    "summary": """   
        nice theme
    """,
    "sequence": 7,
    "license": "OPL-1",
    "category": "Themes/Backend",
    "website": "https://www.terabits.xyz",
    "depends": ["web", "website", "base_setup"],
    "data": [
        "views/webclient_templates.xml",
        'views/icons.xml',
        "views/res_config_setting.xml",
        "views/res_users.xml",
        

    ],
    "assets": {
        "web.assets_backend": [
            "clarity_backend_theme_bits/static/src/newfont/newfont2/fonts/JosefinSans/*.css",
            "clarity_backend_theme_bits/static/src/xml/WebClient.xml",
            "clarity_backend_theme_bits/static/src/xml/navbar/sidebar.xml",
            "clarity_backend_theme_bits/static/src/xml/systray_items/SidebarBottom.xml",
            "clarity_backend_theme_bits/static/src/js/SidebarBottom.js",
            "clarity_backend_theme_bits/static/src/xml/systray_items/user_menu.xml",
            "clarity_backend_theme_bits/static/src/js/WebClient.js",
            "clarity_backend_theme_bits/static/src/css/style.css",
            "clarity_backend_theme_bits/static/src/scss/layout.scss",
            "clarity_backend_theme_bits/static/src/scss/navbar.scss",
            "clarity_backend_theme_bits/static/src/scss/scrollbar.scss",
            "clarity_backend_theme_bits/static/src/scss/form.scss",
            "clarity_backend_theme_bits/static/src/js/navbar.js",
            "clarity_backend_theme_bits/static/src/css/tailwind.css",
        ],
    },
    "installable": True,
    "application": True,
    "auto_install": False,
    'pre_init_hook': 'test_pre_init_hook',
    'post_init_hook': 'test_post_init_hook',
    "images": [
        "static/description/logo.gif",
        "static/description/theme_screenshot.gif",
    ],
}
