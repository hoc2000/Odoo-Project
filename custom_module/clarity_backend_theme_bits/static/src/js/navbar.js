/** @odoo-module **/

import { NavBar } from '@web/webclient/navbar/navbar';
import { useService, useBus } from '@web/core/utils/hooks';
import { patch } from "@web/core/utils/patch";
import { useEnvDebugContext } from "@web/core/debug/debug_context";

patch(NavBar.prototype, 'navbar_patch_bits', {
    setup() {
        this._super();
        this.debugContext = useEnvDebugContext();
        this.rpc = useService('rpc');
        this.companyService = useService("company");
        this.currentCompany = this.companyService.currentCompany;
        this.menuService = useService("menu");
    },
    // toggleSidebar(ev) {
    //     $(ev.currentTarget).toggleClass('visible');
    //     $('.nav-wrapper-bits').toggleClass('toggle-show');
    //     if ($('.sidebar-toggle-bits').hasClass('visible')) {
    //         $('#nav-left-arr').hide();
    //         $('#nav-right-arr').show();
    //     }
    //     else {
    //         $('#nav-right-arr').hide();
    //         $('#nav-left-arr').show();
    //     }
    // },
    toggleIdSidebar() {
        $('#sidebar').toggleClass('active-sidebar');
        if ($('.nav-wrapper-bits').hasClass('active-sidebar')) {
            $('#nav-left-arr').hide();
            $('#nav-right-arr').show();
        }
        else {
            $('#nav-right-arr').hide();
            $('#nav-left-arr').show();
        }
    },
});

window.addEventListener("load", (event) => {
    let url = window.location.href;
    // var getLocalStorageData = localStorage.getItem("sidebar-nav-id");
    $('.main_link').each(function () {
        var menu_id = $(this).attr('data-menu');
        var action_id = $(this).attr('data-action-id');
        if(url.includes("menu_id=" + menu_id) && url.includes("action=" + action_id)) {
            $(this).addClass("active");
        }
    });
});

window.addEventListener("hashchange", (event) => {
    let url = window.location.href;
    // var getLocalStorageData = localStorage.getItem("sidebar-nav-id");
    $('.main_link').each(function () {
        var menu_id = $(this).attr('data-menu');
        var action_id = $(this).attr('data-action-id');
        if(url.includes("menu_id=" + menu_id) && url.includes("action=" + action_id)) {
            $(".nav-item").find(".active").removeClass("active");
            $(this).addClass("active");
        }
    });
});
