$(document).ready(function () {
    checkActiveHeader();

    $('li.nav-item').addClass('d-flex');

});

function checkActiveHeader() {
    let url = window.location.href;
    $('li a').each(function () {
        $(this).removeClass('active');
        if (this.href === url) {
            $(this).addClass('active');
        }
    });
}