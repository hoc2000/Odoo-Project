from odoo import fields, models, api
import requests
from telegram.constants import ParseMode
from html2image import Html2Image
import imgkit
from bs4 import BeautifulSoup

tele_user_ids = ['-4185848821']
my_own_user = ['6206103148']
token = '6794130611:AAH13XSZuqaGiWrEPcQRKkY5e79fElciUL8'


def is_not_header(tag):
    return tag.name not in ["h1"] and tag.string

class ModelName(models.Model):
    _name = 'project.update.bot'
    _description = 'Bot send report to telegram'

    name = fields.Char()

    def calling_api_tele(self, title, project_id, user_id, summary, url):
        update_title = str(title)
        project_name = str(project_id)
        user_id = str(user_id)
        if summary:
            summary_send = summary
        else:
            summary_send = ''
        # USING SOUP CONVERT
        soup = BeautifulSoup(summary_send, "html.parser")
        plain_summary_text = []

        # Iterate over each tag in the parsed HTML

        for tag in soup.find_all(is_not_header):
            tag_text = tag.get_text(separator=" ", strip=True)
            # Add the tag text to the list with a line break
            if tag_text in plain_summary_text[-2:]:
                continue
            else:
                plain_summary_text.append(tag_text)
                plain_summary_text.append("\n")

        # Convert the list to a single string
        plain_summary_text = "".join(plain_summary_text)

        # Print the plain text with line breaks
        print(plain_summary_text)

        message_send = (f'<b>- Cập nhật:</b> {update_title} '
                        f'\n<b>- Dự án:</b> {project_name} '
                        f'\n<b>- Người cập nhật:</b> {user_id}'
                        f'\n<b>- Mô tả cập nhật:</b>'
                        f'\n{plain_summary_text}'
                        f'\n Xem chi tiết hơn tại:'
                        f'\n{url}')
        # img_path = 'output.png'
        # # Convert HTML to image
        # imgkit.from_string(summary_send, img_path)

        self.send_telegram_message(message_send)
        # self.send_telegram_image(img_path, message_send)

    @api.model
    def send_telegram_message(self, message):
        global response
        # chat_id = '6206103148'  # This can be a group or a user chat ID
        url = f'https://api.telegram.org/bot{token}/sendMessage'
        for user_id in tele_user_ids:
            payload = {
                'chat_id': user_id,
                'text': message,
                'parse_mode': ParseMode.HTML
            }
            response = requests.post(url, data=payload)

        return response.json()

    @api.model
    def send_telegram_image(self, image_path, caption):
        global response
        url = f'https://api.telegram.org/bot{token}/sendPhoto'
        # for user_id in tele_user_ids:
        payload = {
            # -4185848821 là group chat báo cáo tuần test @@
            'chat_id': '-4185848821',
            'caption': caption
        }
        files = {'photo': open(image_path, 'rb')}
        response = requests.post(url, params=payload, files=files)
        # print("Image sent:", response.json())
