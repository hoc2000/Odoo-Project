from odoo import fields, models, api
import requests
from telegram.constants import ParseMode
from html2image import Html2Image
import imgkit
from bs4 import BeautifulSoup

PM_BAO_CAO_GROUP = ['-4185848821']
HTKT_TASC_GROUP = ['-4178656294']
my_own_user = ['6206103148']
token = '6794130611:AAH13XSZuqaGiWrEPcQRKkY5e79fElciUL8'


def is_not_header(tag):
    return tag.name not in ["h1"] and tag.string


class ModelName(models.Model):
    _name = 'ticket.update.bot'
    _description = 'Bot send ticket that create report to telegram'

    name = fields.Char()

    def calling_api_tele(self, title, project_id, user_id, summary, url):
        ticket_title = str(title)
        project_name = str(project_id)
        user_id = str(user_id)
        summary_send = summary if summary else 'N/A'
        ticket_title = ticket_title or 'N/A'
        project_name = project_name or 'N/A'

        # USING SOUP CONVERT
        soup = BeautifulSoup(summary_send, "html.parser")
        plain_summary_text = []

        # Iterate over each tag in the parsed HTML

        for tag in soup.find_all(is_not_header):
            tag_text = tag.get_text(separator=" ", strip=True)
            # Add the tag text to the list with a line break
            if tag_text in plain_summary_text[-2:]:
                continue
            else:
                plain_summary_text.append(tag_text)
                plain_summary_text.append("\n")

        # Convert the list to a single string
        plain_summary_text = "".join(plain_summary_text)

        # Print the plain text with line breaks
        print(plain_summary_text)

        message_send = (f'----TICKET MỚI ĐƯỢC TẠO RA----'
                        f'\n<b>-Ticket:</b> {ticket_title} '
                        f'\n<b>-Dự án:</b> {project_name} '
                        f'\n<b>-Người tạo:</b> {user_id}'
                        f'\n<b>-Mô tả:</b>'
                        f'\n{plain_summary_text}')
        # img_path = 'output.png'
        # # Convert HTML to image
        # imgkit.from_string(summary_send, img_path)

        self.send_telegram_message(message_send)
        # self.send_telegram_image(img_path, message_send)

    @api.model
    def send_telegram_message(self, message):
        global response
        # chat_id = '6206103148'  # This can be a group or a user chat ID
        url = f'https://api.telegram.org/bot{token}/sendMessage'
        for user_id in HTKT_TASC_GROUP:  # chỉnh sửa user_id chỗ này
            payload = {
                'chat_id': user_id,
                'text': message,
                'parse_mode': ParseMode.HTML
            }
            response = requests.post(url, data=payload)

        return response.json()
