# -*- coding: utf-8 -*-
{
    "name": "ANSV BOT",
    "summary": """
      ANSV BOT 
      """,
    "description": """
        This is the Bot that will assist information of module , and send reports to telegram 
    """,
    "author": "TuHocVu",
    "website": "",
    "sequence": -119,
    "category": "BOT",
    "version": "1.0",
    # any module necessary for this one to work correctly
    "depends": [
        "base",
        "web",
    ],
    # always loaded
    "data": [

    ],
    "demo": [],
    "installable": True,
    "application": True,
    "auto_install": False,
    "license": "AGPL-3",
}
