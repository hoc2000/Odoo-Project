from odoo import api, fields, models
from datetime import datetime
STATUS_COLOR = {
    'on_track': 10,  # green / success
    'at_risk': 2,  # orange
    'off_track': 1,  # red / danger
    'on_hold': 4,  # light blue
    False: 0,  # default grey -- for studio
    # Only used in project.task
    'to_define': 0,
}


# TELEGRAM


class ProjectUpdates(models.Model):
    _name = 'project.update'
    _description = 'ProjectUpdates'
    _inherit = ['project.update.bot']
    _rec_name = 'title'
    _order = 'create_date desc'

    title = fields.Char(string="Title", help="project update title")
    progress = fields.Integer(string="Progress of the Project", tracking=True)
    progress_percentage = fields.Float(compute='_compute_progress_percentage')
    user_id = fields.Many2one('res.users', string='User Update', required=True, default=lambda self: self.env.user)
    project_id = fields.Many2one('project.ansv', help="Individual projecct update")
    status = fields.Selection([
        ('on_track', 'On Track'),
        ('at_risk', 'At Risk'),
        ('off_track', 'Off Track'),
        ('on_hold', 'On Hold')
    ], string="Status of Task", copy=False, store=True)
    color = fields.Integer(compute='_compute_color')
    update_date = fields.Date(string="Update Date", default=fields.Date.context_today, tracking=True,
                              help="Date that project update the status,...")
    summary = fields.Text(help="This is some description of project udpate status, what update or more info about this")
    result_last_week = fields.Html(help="This is about the result in last week")
    result_this_week = fields.Html(help="This is about the result in this week")
    plan_this_week = fields.Html()
    plan_next_week = fields.Html()
    issue = fields.Html()
    solution = fields.Html()

    @api.depends('status')
    def _compute_color(self):
        for update in self:
            update.color = STATUS_COLOR[update.status]

    @api.depends('progress')
    def _compute_progress_percentage(self):
        for u in self:
            u.progress_percentage = u.progress / 100

    # ---------------------------
    # Change update status
    # ---------------------------
    @api.model_create_multi
    def create(self, vals_list):
        updates = super().create(vals_list)
        print(updates)
        for update in updates:
            print(update.project_id.project_name)
            update.project_id.sudo().last_update_id = update
        # send telegram when create udpate
        # THis def was from ansv bot module
        record = updates.env['project.update'].browse(updates.id)  # Replace 'your.model' with the actual model name
        record_url = self.env['ir.config_parameter'].sudo().get_param(
            'web.base.url') + '/web#id=%s&view_type=form&model=%s' % (updates.id, 'project.update')

        updates.calling_api_tele(updates.title, updates.project_id.project_name, updates.user_id.name, updates.summary,
                                 record_url)
        return updates

    def copy(self, default=None):
        if default is None:
            default = {}
        default['title'] = self.title + '(copy)'
        default['update_date'] = datetime.today().date()
        return super(ProjectUpdates, self).copy(default=default)

    def send_this_update(self):
        for rec in self:
            record = rec.env['project.update'].browse(rec.id)  # Replace 'your.model' with the actual model name
            record_url = self.env['ir.config_parameter'].sudo().get_param(
                'web.base.url') + '/web#id=%s&view_type=form&model=%s' % (rec.id, 'project.update')
            rec.calling_api_tele(rec.title, rec.project_id.project_name, rec.user_id.name, rec.summary,
                                 record_url)
