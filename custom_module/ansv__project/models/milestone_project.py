# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from collections import defaultdict

from odoo import api, fields, models


class ProjectMilestone(models.Model):
    _name = 'project.ansv.milestone'
    _description = "Project ANSV Milestone"
    _inherit = ['mail.thread']
    _order = 'deadline, is_reached desc, name'

    def _get_default_project_id(self):
        return self.env.context.get('default_project_id') or self.env.context.get('active_id')

    name = fields.Char(required=True)
    project_id = fields.Many2one('project.ansv', required=True, default=_get_default_project_id, ondelete='cascade')
    deadline = fields.Date(tracking=True, copy=False)
    is_reached = fields.Boolean(string="Reached", default=False, copy=False)
    reached_date = fields.Date(compute='_compute_reached_date', store=True)
    task_ids = fields.One2many('task.project.ansv', 'milestone_ids', 'Tasks')

    # computed non-stored fields
    # is_deadline_overdue = fields.Boolean(compute="_compute_is_deadline_overdue")

    # is_deadline_future = fields.Boolean(compute="_compute_is_deadline_future")
    # task_count = fields.Integer('# of Tasks', compute='_compute_task_count', groups='project.group_project_milestone')
    # can_be_marked_as_done = fields.Boolean(compute='_compute_can_be_marked_as_done',
    #                                        groups='project.group_project_milestone')

    @api.depends('is_reached')
    def _compute_reached_date(self):
        for ms in self:
            if ms.is_reached:
                ms.reached_date = fields.Date.context_today(self)

    # @api.depends('is_reached', 'deadline')
    # def _compute_is_deadline_overdue(self):
    #     today = fields.Date.context_today(self)
    #     for ms in self:
    #         # nếu nó chưa reach đến milestone này và có deadline và deadline đang bé hơn ngày hôm nay thì overdue là True
    #         ms.is_deadline_overdue = not ms.is_reached and ms.deadline and ms.deadline < today

    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        if default is None:
            default = {}
        milestone_copy = super(ProjectMilestone, self).copy(default)
        if self.project_id.allow_milestones:
            milestone_mapping = self.env.context.get('milestone_mapping', {})
            milestone_mapping[self.id] = milestone_copy.id
        return milestone_copy
