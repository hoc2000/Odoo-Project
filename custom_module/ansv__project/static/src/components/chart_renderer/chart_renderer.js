/** @odoo-module */

import { registry } from "@web/core/registry"
import { loadJS, loadCSS } from "@web/core/assets"
const { Component, onWillStart, useRef, onMounted, useEffect, onWillUnmount } = owl
import { useService } from '@web/core/utils/hooks';

export class ChartRenderer extends Component {
    setup() {
        this.chartRef = useRef("chart")
        this.actionService = useService("action")

        onWillStart(async () => {
            await loadJS("https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.4.0/chart.umd.min.js")
            // await loadJS("web/static/lib/Chart/Chart.js")
            // await loadCSS("https://unpkg.com/aos@2.3.1/dist/aos.css")
            // await loadJS("https://unpkg.com/aos@2.3.1/dist/aos.js")
        })

        onMounted(() => this.renderChart())
        onWillUnmount(() => {
            if (this.chart) {
                this.chart.destroy()
            }
        })

        useEffect(() => {
            this.renderChart()
        }, () => [this.props.config])
    }

    renderChart() {
        const old_chartjs = document.querySelector('script[src="/web/static/lib/Chart/Chart.js"]')
        //lấy chart khác//
        if (old_chartjs) {
            return
        }

        if (this.chart) {
            this.chart.destroy()
        }
        //lấy reverse chart

        this.chart = new Chart(this.chartRef.el,
            {
                type: this.props.type,
                data: this.props.config.data,
                options: this.props.config.options
            }
        );
    }
}

ChartRenderer.template = "owl.ChartRenderer"