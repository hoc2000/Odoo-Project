/** @odoo-module */

import { registry } from "@web/core/registry"
import { KpiCardProject } from "../kpi_card/kpi_card"
import { ChartRenderer } from "../chart_renderer/chart_renderer"
import { loadJS } from "@web/core/assets"
import { useService } from '@web/core/utils/hooks';
const { Component, onWillStart, useRef, onMounted, useState } = owl
//lấy color không bị trùng
import { getColor } from "@web/views/graph/colors"
//this will select the current web right now
var session = require('web.session');
//reload page
import { browser } from "@web/core/browser/browser"
import { routeToUrl } from "@web/core/browser/router_service"

export class OwlProjectDashboard extends Component {
    //Top Project (5 project limit)
    async getTopProject() {
        let domain = []
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }
        const data = await this.orm.readGroup(this.model, domain, ['project_id'], ['project_id'], { limit: 5, orderby: "create_date desc" })
        const project_data = await this.orm.searchRead('project.ansv', [], ['color'])
        // console.log(data)
        const color = ['#dadada', '#f06050', '#f4a460', '#f7cd1f', '#6cc1ed', '#814968', '#eb7b7f', '#2c8397', '#475577', '#d6145f', '#30c381', '#9365b8']

        this.state.topProject = {
            data: {
                labels: data.map(d => d.project_id[1]),
                datasets: [
                    {
                        label: 'Total Tasks',
                        data: data.map(d => d.project_id_count),
                        hoverOffset: 4,
                        backgroundColor: project_data.map(d => color[d.color]),

                    },

                ]
            }
        }
    }
    // Product of Project
    async getTopProductinProject() {
        const color = ['#dadada', '#f06050', '#f4a460', '#f7cd1f', '#6cc1ed', '#814968', '#eb7b7f', '#2c8397', '#475577', '#d6145f', '#30c381', '#9365b8']
        let domain = []
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }
        const data = await this.orm.readGroup('product.ansv', domain, ['project_id'], ['project_id'], { limit: 5, orderby: "create_date desc" })
        const project_data = await this.orm.searchRead('project.ansv', [], ['color'])
        this.state.topProduct = {
            data: {
                labels: data.map(d => d.project_id[1]),
                datasets: [
                    {
                        label: 'Total Products',
                        data: data.map(d => d.project_id_count),
                        hoverOffset: 4,
                        backgroundColor: project_data.map(d => color[d.color]),
                    },

                ]
            }
        }
    }

    //Tassk by Status
    async getTaskbyStatus() {
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];

        let ZeroValMonthOT = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] //OT = Open-Tasks
        let ZeroValMonthRT = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] //CT = Resolved-Tasks
        let ZeroValMonthCT = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] //CT = Closed-Tasks

        let domain = []
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }
        const openTasks = await this.orm.readGroup('task.project.ansv', [['stage_id', 'in', ['In-Progress', 'Open', 'Pending']]], ['created_month'], ['created_month'])
        // console.log(openTasks, 'OPEN TASKS...');
        const resolvedTasks = await this.orm.readGroup('task.project.ansv', [['stage_id', 'in', ['Resolved']]], ['created_month'], ['created_month'])
        const closedTasks = await this.orm.readGroup('task.project.ansv', [['stage_id', 'in', ['Closed']]], ['created_month'], ['created_month'])

        let OpenTasksArr = []
        let ResolvedTasksArr = []
        let ClosedTasksArr = []
        // Parse the dates to JS

        openTasks.forEach((d) => {
            ZeroValMonthOT[d.created_month - 1] = d.created_month_count;
            OpenTasksArr = ZeroValMonthOT
        });

        resolvedTasks.forEach((d) => {
            ZeroValMonthRT[d.created_month - 1] = d.created_month_count;
            ResolvedTasksArr = ZeroValMonthRT
        });

        closedTasks.forEach((d) => {
            ZeroValMonthCT[d.created_month - 1] = d.created_month_count;
            ClosedTasksArr = ZeroValMonthCT
        });
        this.state.taskByStatus = {
            data: {
                labels: monthNames,
                datasets: [
                    {
                        label: 'Open Tasks',
                        data: OpenTasksArr,
                        hoverOffset: 4,
                        backgroundColor: "#80D4FF",
                        borderRadius: 5,
                        // borderColor: "#417690",
                    },
                    {
                        label: 'Resolved Tasks',
                        data: ResolvedTasksArr,
                        hoverOffset: 4,
                        backgroundColor: "#ffc178",
                        borderRadius: 5,
                        // borderColor: "#ffc178",
                    },
                    {
                        label: 'Closed Tasks',
                        data: ClosedTasksArr,
                        hoverOffset: 4,
                        backgroundColor: "#ff8e75",
                        borderRadius: 5,
                        // borderColor: "#ff8e75",
                    },
                ]
            },
            options: {
                layout: {
                    padding: 26
                },
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        grid: {
                            display: false,  // Hide X-axis grid lines
                        }
                    },
                },
                onClick: (e) => {
                    try {
                        const active = e.chart.getActiveElements()
                        if (active) {
                            const label = e.chart.data.labels[active[0].index]
                            this.actionService.doAction({
                                type: "ir.actions.act_window",
                                name: 'Tasks by status',
                                res_model: "task.project.ansv",
                                views: [
                                    [false, "list"],
                                    [false, "kanban"],
                                    [false, "form"],
                                ]
                            })
                        }
                    } catch (error) {
                        console.log("lmao");
                    }
                   
                },
                responsive: true,
                plugins: {
                    legend: {
                        display: false,
                    },

                }
            },
        }
    }
    //Priority Tasks bar
    async getTaskbyPriority() {
        let domain = []
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }
        let medium_tasks_count = await this.orm.searchCount('task.project.ansv', [['priority', '=', '1']])
        let high_tasks_count = await this.orm.searchCount('task.project.ansv', [['priority', '=', '2']])
        let urgent_tasks_count = await this.orm.searchCount('task.project.ansv', [['priority', '=', '3']])
        console.log(medium_tasks_count, urgent_tasks_count, high_tasks_count)
        this.state.taskPriority = {
            data: {
                labels: ['Medium', 'High', 'Urgent'],
                datasets: [
                    {
                        label: 'Priority Tasks',
                        data: [medium_tasks_count, urgent_tasks_count, high_tasks_count],
                        hoverOffset: 4,
                        backgroundColor: ["#80D4FF", "#ffc178","#ff8e75"]
                    },
                ]
            },
            options: {
                scales: {
                    y: {
                        display: false,  // Hide X-axis grid lines
                    },
                    x: {
                        display: false,  // Hide X-axis grid lines
                    },
                },
                layout: {
                    padding: 26
                },
                plugins: {
                    legend: {
                        display: true,
                        position: 'right',
                    },

                }
                // cutout: 150
            },
        }
    }

    // async getTaskbyPriority() {
    //     let domain = []
    //     if (this.state.period > 0) {
    //         //domain.push(['create_date', '>', this.state.date])
    //     }
    //     const data = await this.orm.searchRead('project.ansv', domain, ['project_name', 'medium_tasks', 'high_tasks', 'urgent_tasks'])
    //     // console.log(data)
    //     this.state.taskPriority = {
    //         data: {
    //             labels: data.map(d => d.project_name),
    //             datasets: [
    //                 {
    //                     label: 'Medium',
    //                     data: data.map(d => d.medium_tasks),
    //                     hoverOffset: 4,
    //                     backgroundColor: "#a9f451",
    //                 },
    //                 {
    //                     label: 'High',
    //                     data: data.map(d => d.high_tasks),
    //                     hoverOffset: 4,
    //                     backgroundColor: "#fcd382",
    //                 },
    //                 {
    //                     label: 'Urgent',
    //                     data: data.map(d => d.urgent_tasks),
    //                     hoverOffset: 4,
    //                     backgroundColor: "#f56c6c",
    //                 },
    //             ]
    //         }
    //     }
    // }

    //SET UP
    setup() {
        this.state = useState({
            uid: null,
            task: { value: 0, percentage: 0, },
            taskList: [],
            incompletedtask: { value: 0, percentage: 0, },
            completedtask: { value: 0, percentage: 0, },
            mytask: { value: 0, percentage: 0, },
            period: 90,
        })
        this.model = "task.project.ansv"
        this.orm = useService("orm")
        this.actionService = useService("action")
        /*load chartjs*/
        const old_chartjs = document.querySelector('script[src="/web/static/lib/Chart/Chart.js"]')
        const router = useService("router")

        if (old_chartjs) {
            let { search, hash } = router.current
            search.old_chartjs = old_chartjs != null ? "0" : "1"
            hash.action = 170
            browser.location.href = browser.location.origin + routeToUrl(router.current)
        }
        /*load chartjs*/
        onWillStart(async () => {
            this.getDates()
            await this.GetAllTask()
            // console.log(this.state.taskList)
            await this.GetIncompletedTask()
            await this.GetCompletedTask()
            await this.GetMyTask()
            await this.GetAllProjects()
            //chart
            await this.getTopProject()
            await this.getTopProductinProject()
            await this.getTaskbyStatus()
            await this.getTaskbyPriority()
        })

    }
    //All Project
    async GetAllProjects() {
        let domain = []
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }
        const data = await this.orm.searchRead('project.ansv', domain, ['project_name', 'manager_id', 'last_update_progress','stage_id','write_date'])
        const user = await this.orm.searchRead('res.users', [['share', '=', false]], ['name','image_1920'])
        console.log('USERS:',user,'and DATA:',data)
        this.state.projects = data
        this.state.users = user
        console.log(user)
    }

    //All tasks
    async GetAllTask() {
        let domain = []
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }
        const countTasks = await this.orm.searchCount(this.model, domain)
        this.state.task.value = countTasks
    }
    viewTasks() {
        let domain = []
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }

        this.actionService.doAction({
            type: "ir.actions.act_window",
            name: "All Tasks",
            res_model: this.model,
            domain, //same name as domain so dont need to change add value
            views: [
                [false, "list"],
                [false, "kanban"],
                [false, "form"],
            ]
        })
    }


    //incomlpeted tasks
    async GetIncompletedTask() {
        //domain default can push though :))
        let domain = [['stage_id', 'in', ['In-Progress', 'Open', 'Pending']]]
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }
        const incompletedTasks = await this.orm.searchCount(this.model, domain)
        this.state.incompletedtask.value = incompletedTasks
    }


    viewIncompletedTasks() {
        let domain = [['stage_id', 'in', ['In-Progress', 'Open', 'Pending']]]
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }

        this.actionService.doAction({
            type: "ir.actions.act_window",
            name: "Incompleted Tasks",
            res_model: this.model,
            domain, //same name as domain so dont need to change add value
            views: [
                [false, "list"],
                [false, "kanban"],
                [false, "form"],
            ]
        })
    }

    //Completed Tasks
    async GetCompletedTask() {
        //domain default can push though :))
        let domain = [['stage_id', 'in', ['Resolved', 'Closed']]]
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }
        const completedTasks = await this.orm.searchCount(this.model, domain)
        this.state.completedtask.value = completedTasks
    }

    viewCompletedTasks() {
        let domain = [['stage_id', 'in', ['Resolved', 'Closed']]]
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }

        this.actionService.doAction({
            type: "ir.actions.act_window",
            name: "Completed Tasks",
            res_model: this.model,
            domain, //same name as domain so dont need to change add value
            views: [
                [false, "list"],
                [false, "kanban"],
                [false, "form"],
            ]
        })
    }
    //Personal Tasks
    async GetMyTask() {
        //domain default can push though :))
        const uid = session.uid
        this.state.uid = uid;
        let domain = [['assignees_id', 'in', this.state.uid]]
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }
        const myTasks = await this.orm.searchCount(this.model, domain)
        this.state.mytask.value = myTasks
    }

    viewMyTasks() {
        let domain = [['assignees_id', 'in', this.state.uid]]
        if (this.state.period > 0) {
            //domain.push(['create_date', '>', this.state.date])
        }

        this.actionService.doAction({
            type: "ir.actions.act_window",
            name: "My Tasks",
            res_model: this.model,
            domain, //same name as domain so dont need to change add value
            views: [
                [false, "list"],
                [false, "kanban"],
                [false, "form"],
            ]
        })
    }

    getDates() {
        this.state.date = moment().subtract(this.state.period, 'days').format('L');
    }

    async onChangePeriod() {
        //sử dụng mement của js
        this.getDates()
        await this.GetAllTask()
        await this.GetIncompletedTask()
        await this.GetCompletedTask()
        await this.GetMyTask()
        await this.GetAllProjects()

        await this.getTopProject()
        await this.getTopProductinProject()
        await this.getTaskbyStatus()
        await this.getTaskbyPriority()
    }
}

OwlProjectDashboard.template = "owl.ProjectDashboard"
OwlProjectDashboard.components = { KpiCardProject, ChartRenderer }
registry.category("actions").add("owl.project_dashboard", OwlProjectDashboard)




