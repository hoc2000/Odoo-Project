
$(document).ready(function () {
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form ...
        var $x = $(".tab");
        console.log('the tab', $x);
        $x.eq(n).show();
        // ... and fix the Previous/Next buttons:
        if (n === 0) {
            $("#prevBtn").hide();
            $("#submitBtn").hide();
        } else {
            $("#prevBtn").show();
        }
        if (n === $x.length - 1) {
            $("#nextBtn").hide();
            $("#submitBtn").show();
            // $("#nextBtn").removeAttr("onclick");
            // $("#nextBtn").html("Submit").attr("type", "submit");
        } else {
            $("#nextBtn").show();
        }
        // ... and run a function that displays the correct step indicator:
        fixStepIndicator(n);
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var $x = $(".tab");
        // Exit the function if any field in the current tab is invalid:
        if (n === 1 && !validateForm()) return false;
        // Hide the current tab:
        $x.eq(currentTab).hide();
        // Increase or decrease the current tab by 1:
        currentTab += n;
        // if you have reached the end of the form... :
        if (currentTab >= $x.length) {
            //...the form gets submitted:
            // $("#regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var $x, $y, valid = true;
        $x = $(".tab");
        $y = $x.eq(currentTab).find("input");
        // A loop that checks every input field in the current tab:
        $y.each(function () {
            // If a field is empty...
            if ($(this).val() === "") {
                // add an "invalid" class to the field:
                $(this).addClass("invalid");
                // and set the current valid status to false:
                valid = false;
            }
        });
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            $(".step").eq(currentTab).addClass("finish");
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var $x = $(".step");
        $x.removeClass("active");
        //... and adds the "active" class to the current step:
        $x.eq(n).addClass("active");
    }

    // Event handler for previous button
    $("#prevBtn").on("click", function () {
        nextPrev(-1);
    });

    // Event handler for next button
    $("#nextBtn").on("click", function () {
        nextPrev(1);
    });
    // Your JavaScript code goes here
    // Example: Attach an event listener to an input field
    $('#ticketForm').on('submit', function (event) {
        let preloader = $('#preloader');
        preloader.show();

        event.preventDefault(); // Prevent default form submission
        var formData = $(this).serialize(); // Serialize form data

        $.ajax({
            url: '/create/webticket',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function (response) {
                // Handle the response
                preloader.hide();
                console.log(response.message);
                $('#statusSuccessModal').modal('show');
            },
            error: function (xhr, status, error) {
                // Handle errors
                preloader.hide();
                console.error('Error:', xhr.responseText);
                $('#statusErrorsModal').modal('show');
            }
        });
    });



    $("#project_id").on('change', function () {
        var selectedValue = $(this).val();
        // Make an AJAX request to fetch options for Selector 2 based on selectedValue
        $.ajax({
            type: "POST",
            url: "/my/get_options",
            dataType: "json",
            data: {
                project_value: selectedValue,
            },
            success: function (res) {
                // Update options for Selector 2 based on the response
                console.log(res);
                var fieldSelection2 = $('#product');
                fieldSelection2.empty();
                // empty
                var emptyElement = $('<option>').val("").text("");
                fieldSelection2.append(emptyElement);
                //empty
                _.each(res, function (option) {
                    var optionElement = $('<option>').val(option.value).text(option.text);
                    fieldSelection2.append(optionElement);
                });
                $('#component').empty();
                $('#component').append($('<option>').val('').text(''));
            },
        });
    });

    $("#product").on('change', function () {
        var selectedValue = $(this).val();
        // Make an AJAX request to fetch options for Selector 2 based on selectedValue
        $.ajax({
            type: "POST",
            url: "/my/get_components",
            dataType: "json",
            data: {
                product_value: selectedValue,
            },
            success: function (res) {
                // Update options for Selector 2 based on the response
                console.log(res);
                var fieldSelection2 = $('#component');
                fieldSelection2.empty();
                fieldSelection2.append($('<option>').val('').text(''));
                _.each(res, function (option) {
                    var optionElement = $('<option>').val(option.value).text(option.text);
                    fieldSelection2.append(optionElement);
                });
            },
        });
    });


});