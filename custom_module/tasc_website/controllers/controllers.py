# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.portal.controllers.portal import CustomerPortal, pager
import json
from odoo.addons.website.controllers.main import Website


class Mockdesk(http.Controller):
    @http.route('/mockdesk/tickets', auth='user', type="json")
    def team_ticket_banner(self, **kw):
        return {
            'html': """
            <div>
                <center>
                <h1>TU HOC VU TEST</h1>
                </center>            
            </div>
            """
        }

    @http.route('/', type="http", auth="public", website=True)
    def homepage(self, **kw):
        return http.request.render('tasc_website.new_homepage', {})

    @http.route('/ticket_webform', type="http", auth="user", website=True)
    def ticketWebform(self, **kw):
        user_partner_id = request.env.user.partner_id
        print("Execution Here.........................")
        # ticket_rec = request.env['mockdesk.ticket'].sudo().search([], limit=1)
        project_rec = request.env['project.ansv'].sudo().search([('partner_ids', '=', user_partner_id.id)])
        product_rec = request.env['product.ansv'].sudo().search([])
        version_rec = request.env['component.version'].sudo().search([])
        component_rec = request.env['component.product.ansv'].sudo().search([])
        print("Ticket rec...", component_rec)
        return http.request.render('tasc_website.create_ticket', {'partner_id': user_partner_id,
                                                               'project_rec': project_rec,
                                                               'product_rec': product_rec,
                                                               'version_rec': version_rec,
                                                               'component_rec': component_rec})

    # ONCHANGE SELECTION
    @http.route('/my/get_options', type='http', auth="public", website=True, csrf=False)
    def get_options(self, **kw):
        project_value = kw.get('project_value')
        # print(project_value)
        if not project_value == '':
            product_rec = request.env['product.ansv'].sudo().search([('project_id', '=', int(project_value))])
            # print(product_rec)

            # Perform any logic to generate options for Selector 2 based on selected_value
            product_selector = []
            for rec in product_rec:
                value_temp = {'value': rec.id, 'text': rec.product_name}
                product_selector.append(value_temp)
        else:
            product_selector = []

        # print(product_selector)
        return json.dumps(product_selector)

    @http.route('/my/get_components', type='http', auth="public", website=True, csrf=False)
    def get_components(self, **kw):
        product_value = kw.get('product_value')
        # print(project_value)
        if not product_value == '':
            component_rec = request.env['component.product.ansv'].sudo().search(
                [('product_id', '=', int(product_value))])
            # print(product_rec)

            # Perform any logic to generate options for Selector 2 based on selected_value
            component_selector = []
            for rec in component_rec:
                value_temp = {'value': rec.id, 'text': rec.name}
                component_selector.append(value_temp)
        else:
            component_selector = []

        # print(product_selector)
        return json.dumps(component_selector)

    # CREATE TICKET
    @http.route('/create/webticket', type="http", auth="user", csrf=True, website=True)
    def ticketCreate(self, **kw):
        # print('Data Raw......', kw)
        # pid = kw.get('project_id')
        # project_find = request.env['project.ansv'].sudo().search([('id', '=', pid)])
        kw.update({'team_id': False})
        # Create new customer
        customer_name = kw.get('customer_name')
        customer_phone = kw.get('phone')
        customer_mail = kw.get('email')
        customer_country = kw.get('country')
        keys_to_remove = ['customer_name', 'phone', 'email', 'product', 'country', 'company', 'product_component']

        # Using a loop to remove keys
        for key in keys_to_remove:
            kw.pop(key, None)

        existed_customer = request.env['res.partner'].sudo().search([('name', '=', customer_name)], limit=1, )
        default_stage = request.env['mockdesk.stage'].sudo().search([('name', '=', 'New')], limit=1)
        if not existed_customer:
            customer_vals = {'name': customer_name, 'phone': customer_phone, 'email': customer_mail, }

            new_customer = request.env['res.partner'].sudo().create(customer_vals)

            values = {'customer_id': new_customer.id, 'stage_id': default_stage.id}
        else:
            values = {'customer_id': existed_customer.id, 'stage_id': default_stage.id}

        kw.update(values)
        print("Data Received.....", kw)
        sla_add = request.env['sla.policy.ansv'].sudo().search([
            ('priority', '=', str(kw['priority'])),
            ('project_id', '=', int(kw['project_id']))
        ])
        print("Sla founded.....", sla_add)

        new_ticket = request.env['mockdesk.ticket'].create(kw)
        sla_status_id, working_time_total = request.env['mockdesk.ticket'].process_sla(new_ticket, sla_add)
        print("SLA STATUS:....", sla_status_id, working_time_total)
        ticket_id = new_ticket.id
        if sla_status_id:
            for sla_id in sla_status_id:
                query = '''
                        INSERT INTO individual_ticket_sla_mockdesk_ticket_rel(mockdesk_ticket_id, individual_ticket_sla_id)
                        VALUES (%(ticket_id)s, %(sla_id)s)
                '''
                params = {
                    'ticket_id': ticket_id,
                    'sla_id': sla_id
                }
                request.cr.execute(query, params)
        else:
            print("there is no SLA found")

        new_ticket.sudo().write({
            # 'sla_status_id': [(6, 0, sla_status_id)],
            'working_time_total': working_time_total,
        })

        response_data = {
            'message': 'Ticket created successfully!',
            # Add additional data if needed
        }
        return json.dumps(response_data)

    # @http.route(auth='public', website=True)
    # def index(self, **kw):
    #     return http.request.render('tasc_website.new_homepage', {})


class Mockdesk_Product(CustomerPortal):
    # PRODUCT
    @http.route(['/product', '/product/page/<int:page>'], auth="public", type="http", website=True)
    def get_product_list(self, page=1, **kw):
        total_product_rec = request.env['product.ansv'].sudo().search_count([])
        print(total_product_rec)
        page_detail = pager(url='/product',
                            total=total_product_rec,
                            page=page,
                            step=6)
        product_rec = request.env['product.ansv'].sudo().search([], limit=6, offset=page_detail['offset'])

        return http.request.render("tasc_website.product_grid_view_customer",
                                   {'products': product_rec, 'pager': page_detail})

    @http.route(['/product/<int:product_id>'], type="http", auth="public", website=True)
    def get_detail_product(self, product_id, **kw):
        product_val = request.env['product.ansv'].sudo().search([('id', '=', product_id)])
        vals = {'product': product_val}

        return request.render('tasc_website.product_detail_view', vals)

    # PROJECT
    @http.route(['/project', '/project/page/<int:page>'], auth="public", type="http", website=True)
    def get_project_list(self, page=1, **kw):
        user_partner_id = request.env.user.partner_id.id

        total_project_rec = request.env['project.ansv'].sudo().search_count([('partner_ids', '=', user_partner_id)])
        print(total_project_rec)
        page_detail = pager(url='/project',
                            total=total_project_rec,
                            page=page,
                            step=4)
        project_rec = request.env['project.ansv'].sudo().search([('partner_ids', '=', user_partner_id)], limit=4,
                                                                offset=page_detail['offset'])

        return http.request.render("tasc_website.project_media_view",
                                   {'projects': project_rec, 'pager': page_detail})

    @http.route(['/project/<int:project_id>'], type="http", auth="public", website=True)
    def get_detail_project(self, project_id, **kw):
        product_val = request.env['project.ansv'].sudo().search([('id', '=', project_id)])
        product_in_project = request.env['product.ansv'].sudo().search([('project_id', '=', project_id)])
        print(product_in_project)
        vals = {'project': product_val, 'products': product_in_project}

        return request.render('tasc_website.project_detail_view', vals)
