# -*- coding: utf-8 -*-
{
    "name": "Tasc Website",
    "summary": """
        This is website theme for tasc, everything will be fix here 
      """,
    "description": """
        Be careful with the model and controller to changes
    """,
    "author": "Hoc Vu Tu",
    "website": "https://ansv.vn",
    "category": "tasc",
    "version": "1.0",
    # any module necessary for this one to work correctly
    "sequence": -1,
    "depends": ["base", "web", "website"],
    "installable": True,
    # always loaded
    "data": [
        # security
        # "security/ir.model.access.csv",
        "views/website_header.xml",
        "views/website_form.xml",
        "views/website_homepage.xml",
        "views/website_product.xml",
        "views/website_nav_footer.xml",
        "views/website_project.xml",
    ],
    # only loaded in demonstration mode
    "demo": [],
    "assets": {
        "web.assets_frontend": [
            "tasc_website/static/src/scss/login.scss",
            "tasc_website/static/src/css/front-css/flickity.min.css",
            "tasc_website/static/src/css/front-css/form_customer.css",
            "tasc_website/static/src/css/front-css/modal.bootsrap.css",
            "tasc_website/static/src/css/front-css/style.css",
            "tasc_website/static/src/vendor/bootstrap/css/bootstrap.css",
            "tasc_website/static/src/vendor/bootstrap-icons/bootstrap-icons.css",
            "tasc_website/static/src/newfont/newfont2/fonts/JosefinSans/*.css",
            "tasc_website/static/src/scss/front-scss/front_end.scss",
            # vendor-css
            # "tasc_website/static/src/vendor/aos/aos.css",
            # "tasc_website/static/libs/aos/dist/aos.css",
            "tasc_website/static/src/vendor/boxicons/css/boxicons.min.css",
            "tasc_website/static/src/vendor/glightbox/css/glightbox.min.cs",
            "tasc_website/static/src/vendor/remixicon/remixicon.css",
            "tasc_website/static/src/vendor/swiper/swiper-bundle.min.css",
            "tasc_website/static/src/vendor/assets/custom-style-2.css",
            #
            "tasc_website/static/src/js/frontjs/form_customer.js",
            "tasc_website/static/src/js/frontjs/flickity.pkgd.min.js",
            "tasc_website/static/src/js/frontjs/header.js",
            # # vendor-js
            # "tasc_website/static/src/vendor/aos/aos.js",
            # "tasc_website/static/src/vendor/assets/main.js",
            # "tasc_website/static/src/vendor/glightbox/js/glightbox.min.js",
            # "tasc_website/static/src/vendor/isotope-layout/isotope.pkgd.min.js",
            # "tasc_website/static/src/vendor/swiper/swiper-bundle.min.js",
            # "tasc_website/static/src/vendor/waypoints/noframework.waypoints.js",
        ],
        "web.assets_backend" :[]
    },
}
