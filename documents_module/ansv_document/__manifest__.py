# -*- coding: utf-8 -*-
{
    "name": "ansv_document",
    "summary": """
        This will be the module to save the information about the file upload""",
    "author": "TuHocVu",
    "website": "https://www.ansv.vn",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/16.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "Document Management",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "ansv__project"],
    # always loaded
    "data": [
        "security/ir.model.access.csv",
        "security/group_security_access.xml",
        # 'views/template_asset.xml',
        "views/menu.xml",
        "views/file.xml",
        "views/folder.xml",
        "views/configure.xml",
    ],
    # only loaded in demonstration mode
    "demo": [
        "demo/demo.xml",
    ],
    "assets": {
        "web.assets_backend": [
            "ansv_document/static/src/css/*.css",
            "ansv_document/static/src/components/*/*.js",
            "ansv_document/static/src/components/*/*.xml",
            "ansv_document/static/src/js/*.js",
        ],
        "web.assets_frontend": [],
    },
    "application": True,
    "installable": True,
}
