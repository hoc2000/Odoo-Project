from odoo import _, models, api, fields, tools
import os
import base64
from odoo import tools

# BASE_PATH = "D:/odoo-file/Project-Document-ANSV/"
CONF_BASE_PATH = tools.config.get('odoo.file_save_path')


# BASE_PATH = "/home/ansv/.local/share/Odoo/filestore/Project-Document-ANSV/"

class File(models.Model):
    _name = 'ansv.file'
    _description = 'File'
    _order = 'create_date asc'

    name = fields.Char(required=True)
    active = fields.Boolean(string="Archived", default=True,
                            help="If a file is set to archived, it is not displayed, but still exists.")
    project_id = fields.Many2one('project.ansv', string="Project")
    attachment_data = fields.Binary(string="Attachment file", attachment=True)
    file_name = fields.Char('File Name')
    attachment_id = fields.Many2one('ir.attachment', string='Attachment id', ondelete='cascade')
    file_size = fields.Integer('File Size', related="attachment_id.file_size", store=True)
    # related fields
    id_image = fields.Integer(related="attachment_id.id")
    mimetype = fields.Char(related="attachment_id.mimetype")
    type = fields.Selection(related="attachment_id.type")
    file_saved_path = fields.Char('File Saved Path')
    tag_ids = fields.Many2many('tags.ansv.document', 'rel_tag_file', 'tag_id', 'file_id', 'tag')
    # is document
    is_folder = fields.Boolean('Is Folder', default=False)
    is_child = fields.Boolean('Is Child', compute="check_child_document", search='_search_is_child', store="True")
    parent_id = fields.Many2one('ansv.file', 'In Folder', ondelete="cascade")
    child_ids = fields.One2many('ansv.file', 'parent_id', string='Child Documents')
    documents_count = fields.Integer('Number of documents', compute="get_document_contain_count")

    def click_through(self):
        pass

    # _sql_constraints = [
    #     ('name_uniq', 'unique (name)', 'The File name already upload here!')
    # ]
    def get_document_contain_count(self):
        for rec in self:
            count_doc = rec.env['ansv.file'].search_count(
                [('parent_id', '=', rec.id)])
            rec.documents_count = count_doc

    @api.depends('parent_id')
    def check_child_document(self):
        for rec in self:
            if rec.parent_id:
                rec.is_child = True
            else:
                rec.is_child = False

    def action_view_file(self):
        for rec in self:
            currFolderId = rec.id
            return {
                'name': rec.name,
                'type': 'ir.actions.act_window',
                'view_mode': 'kanban,tree,form',
                'res_model': 'ansv.file',
                # 'view_id': self.env.ref("mockdesk.all_ticket_view_kanban").id,
                'context': {
                    'default_parent_id': rec.id,
                    'default_project_id': rec.project_id.id,
                },
                'domain': [('parent_id', '=', currFolderId)],

            }

    def write(self, vals):
        # print(vals)
        # vals.update({'file_saved_path': ''})
        record = super(File, self).write(vals)
        if 'attachment_data' in vals:
            attachment = self.env['ir.attachment'].search(
                [('id', '=', self.attachment_id.id)])
            attachment.write({
                'name': self.name,
                'datas': vals.pop('attachment_data'),
                'res_model': self._name,
                'res_id': self.id,
            })
            vals['attachment_id'] = attachment.id
            # replace file
            self.remove_file_from_dir()
            vals['file_saved_path'] = ''
            self.save_file_to_filesystem()
        return record

    @api.model
    def create(self, vals):
        # print(CONF_BASE_PATH)
        # attachment_data = vals.pop('attachment_data', False)
        # print(vals)
        record = super(File, self).create(vals)
        record.save_file_to_filesystem()  # lan 1 write
        if vals['attachment_data']:
            attachment = self.env['ir.attachment'].create({
                'name': record.name,
                'datas': vals['attachment_data'],
                'res_model': self._name,
                'res_id': record.id,
            })
            record.attachment_id = attachment.id  # lan 2 write
        return record

    def unlink(self):
        self.remove_file_from_dir()
        return super(File, self).unlink()

    def remove_file_from_dir(self):
        try:
            # Check if the file exists
            if os.path.exists(self.file_saved_path):
                # Remove the file
                os.remove(self.file_saved_path)
                print(f"File '{self.file_saved_path}' has been removed successfully.")
            else:
                print(f"File '{self.file_saved_path}' does not exist.")
        except Exception as e:
            print(f"An error occurred while trying to remove the file: {e}")

    def save_file_to_filesystem(self):
        project_name = self.project_id.project_name
        if project_name:
            base_path = f'{CONF_BASE_PATH}{project_name}'
        else:
            base_path = f'{CONF_BASE_PATH}None-Project-File'

        if not os.path.exists(base_path):
            os.makedirs(base_path)

        for record in self:
            if record.attachment_data and record.name:
                file_path = os.path.join(base_path, record.name)
                with open(file_path, 'wb') as file:
                    file.write(base64.b64decode(
                        record.attachment_data))  # For Odoo 16, use `record.file` directly if it's already binary.
                record.file_saved_path = file_path  # Save the file path if needed
