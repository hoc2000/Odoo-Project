/** @odoo-module **/

import { registry } from "@web/core/registry";
import { kanbanView } from "@web/views/kanban/kanban_view";
import { KanbanController } from "@web/views/kanban/kanban_controller";
import { useService } from '@web/core/utils/hooks';

const { Component, useState, onWillStart, useRef } = owl;


export class TreeNode extends Component {
  setup() {
    // console.log(this.env.searchModel.globalContext.default_project_id);
    this.orm = useService("orm")
    this.actionService = useService("action")
    this.state = useState({
      folders: [],
    });
    onWillStart(async () => {
      await this.getChildNodes(this.props.node.child_ids);
    });
  }
  async getChildNodes(childIds) {
    // console.log(typeof(childIds));
    var childIdsarray = Object.keys(childIds).map(function (key) {
      return childIds[key];
    });
    // console.log(childIdsarray); // Output: [1, 2, 3]
    const childFolder = await this.orm.searchRead('ansv.file', [
      ['is_folder', '=', true],
      ['id', '=', childIdsarray]
    ], ['id', 'name', 'child_ids']);
    this.state.folders = childFolder
  }

  selectFolder(event,folder) {

    if(!$(event.target).hasClass('selected')){
      $('.tree-nav__item-title').removeClass('selected');
      $(event.target).addClass('selected');
    }
    
    const folderId = folder.id
    this.env.searchModel.globalContext.default_parent_id = folderId
    const folderName = folder.name
    console.log(folderId);
    this.env.searchModel.setDomainParts({
      folder: {
        domain: [
          ['parent_id', '=', folderId]
        ],
        facetLabel: folderName,
      }
    })
  }
}

TreeNode.components = {
  TreeNode
}
TreeNode.template = "owl.TreeNode"
