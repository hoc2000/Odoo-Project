/** @odoo-module **/

import { registry } from "@web/core/registry";
import { kanbanView } from "@web/views/kanban/kanban_view";
import { KanbanController } from "@web/views/kanban/kanban_controller";
import { useService } from '@web/core/utils/hooks';
import { TreeNode } from "./treeNode";
const { Component, useState, onWillStart, useRef } = owl;

import { KanbanRecord } from '@web/views/kanban/kanban_record';
import { KanbanRenderer } from '@web/views/kanban/kanban_renderer';
import { CheckBox } from "@web/core/checkbox/checkbox";

let draggedElement = null;
let dropElement = null;

// Track the dragged element
document.addEventListener('dragstart', function(event) {
    draggedElement = event.target.closest('.o_kanban_record');
    // console.log(draggedElement);
});

document.addEventListener('dragend', function(event) {
    draggedElement = null;
});

function formatBytes(bytes) {
    if (bytes === 0) return '0 B';
    const k = 1024;
    const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
}


export const sharedValue = {
    selectDocumentId: '',
    default: {
        default_project_id: '',
        default_parent_id:'',
        },
    model: ''
};

class DocumentsViewController extends KanbanController {
    setup() {
        //set default domain        
        super.setup()
        this.state = useState({
            document: { name: 'test', type:'', file_size:0 , project_id: '', create_uid: 'test' ,tag_ids:[] },
            docId : 0,
            projects: [],
            owners: [],
            tags: [],
            selectedProject: {
                id: '',
                name: '',
            },

            folders:[],
        });
        this.orm = useService("orm")
        this.actionService = useService("action")
        this.parentFodler = [];
        // console.log("Hello from Tu Hoc vu");
        //get context by this 
        
        // this.domain = this.props.domain;

        // set sharedValue
        sharedValue.default.default_project_id = this.project_id;
        sharedValue.default.default_parent_id = this.parent_id;
        sharedValue.model = this.model;
       
        //get sharedValue
        onWillStart(async () => {
            console.log('context',this.env.searchModel.globalContext);
            // this.setToHome()
            //Folder that is parent
            await this.fetchTreeData();
            await this.fetchProject();
            await this.fetchOwner();

            window.addEventListener('contextmenu', (ev) => {
                var elementRecord = $(ev.target).closest('.o_kanban_record');
                this.state.docId = elementRecord.find('.o_documents_allow_context').data('id');
                
                if (this.state.docId) {
                    ev.preventDefault();
                    $('#context-menu').addClass("active");
                    this.showContextMenu(ev);
                    // console.log('lmao right-click',dataId);
                } else {
                    // hideContextMenu();
                    console.log('lmao');

                }
            });

            window.addEventListener('click',(ev)=>{
                var inspector_el = ev.target.closest('.document_inspector');
                if(!inspector_el){
                    $('.document_inspector').removeClass('actiive')
                }
                $("#context-menu").removeClass("active")
                var buttonDetail = ev.target.closest('#detail-doc-btn')
                // console.log(buttonDetail);
                if(buttonDetail){
                    var elementRecord = $(ev.target).closest('.o_kanban_record');
                    this.state.docId = elementRecord.find('.o_documents_attachment').data('id');
                    // console.log(this.state.docId);
                    this.showDetailDoc();
                }
            })
    });
    }
    //Laaya Duwx lieuj 
    async fetchProject() {
        this.state.projects = await this.orm.searchRead('project.ansv', [], ["id", "project_name"])
    }

    async fetchOwner() {
        this.state.owners = await this.orm.searchRead('res.users', [], ["id", "name"])
    }
    // tree transform data
    async fetchTreeData() {
        let childFolder = [];
        let parentFolder = [];
        // console.log(this.env.searchModel.globalContext.default_project_id);
        this.getEnvValue()
        if(this.parent_id){
            parentFolder = await this.orm.searchRead('ansv.file',[['id', '=', this.parent_id]], ['id','name'])
            this.parentFodler = parentFolder[0]
        }

        if(!this.project_id)
        {
            childFolder = await this.orm.searchRead('ansv.file',[['is_folder', '=', true],['parent_id','=',false]], ['id','name','child_ids'])
    
        }else{
            childFolder = await this.orm.searchRead('ansv.file',[['is_folder', '=', true],['parent_id','=',this.parent_id],['project_id','=',this.project_id]], ['id','name','child_ids'])
        }

        
        this.state.folders = childFolder
        console.log(this.state.folders);
    }

    setToHome(event) {
        if(!$(event.target).hasClass('selected')){
            $('.tree-nav__item-title').removeClass('selected');
            $(event.target).addClass('selected');
          }
        $(event.target).removeClass('selected');
        $(event.target).addClass('selected');
        delete this.env.searchModel.globalContext.default_parent_id
        this.env.searchModel.setDomainParts({
            folder: {
                domain: [['parent_id', '=', false]],
                facetLabel:'Home',
            }
          });
    }

    setToParentFolder(event) {
        if(!$(event.target).hasClass('selected')){
            $('.tree-nav__item-title').removeClass('selected');
            $(event.target).addClass('selected');
          }
        $(event.target).removeClass('selected');
        $(event.target).addClass('selected');
        this.env.searchModel.setDomainParts({
            folder: {
                domain: [['parent_id', '=', this.parent_id]],
                facetLabel: this.parentFodler.name,
            }
          });
    }

    getEnvValue(){
        this.project_id = this.env.searchModel.globalContext.default_project_id;
        this.parent_id = this.env.searchModel.globalContext.default_parent_id;
    }

    showContextMenu(ev) {
        let contextMenu = $('#context-menu');
        contextMenu.off('click').on('click', 'div', (event) => {
            event.preventDefault();
            const action = $(event.currentTarget).data('action');
            if (action) {
                this.handleContextMenuAction(action);
            }
        });
    
        contextMenu.css({
            left: `${ev.pageX}px`,
            top: `${ev.pageY}px`
        });
    }
    

    handleContextMenuAction(action){
        switch(action) {
            case 'download':
                this.downloadFile();
                console.log('Download triggered');
                break;
            case 'replace':
                this.ReplaceFile()
                console.log('Replace triggered');
                break;
            // case 'delete':
            //     this.deleteDoc();
            //     console.log('Delete triggered');
            //     break;
            case 'detail':
                this.showDetailDoc();
                console.log('Details triggered');
                break;
            default:
                break;
        }
    }

    selectProject(project) {
        console.log(project.project_name);
        $('#project-select').val(project.project_name)
        $('#project-select').attr('data-id', project.id)

        $('#dropdown-project').css('display', 'none');
    }

    selectOwner(owner) {
        console.log(owner.name);
        $('div[name="owner_id"] input').val(owner.name)
        $('#dropdown-owner').css('display', 'none');
    }

    inputDropDownShow(ev) {
        switch (ev.target.id) {
            case 'project-select':
                $('#dropdown-project').toggle();;
                break;
            case 'owner-select':
                $('#dropdown-owner').toggle();;
                break;
        }
        // dropdownList.style.display = 'block'
    }

    async showDetailDoc() {
        try {
            const document = await this.orm.searchRead('ansv.file', [['id', '=', this.state.docId]], ['name', 'attachment_id', 'mimetype', 'create_uid', 'file_size', 'project_id', 'tag_ids']);
            console.log(document[0]);
    
            // Extracting tag_ids asynchronously
            const tag_ids = await this.FindTagsDoc(document[0].tag_ids);
    
            // Determine file type and set file URL
            this.state.document.type = document[0].mimetype.toLowerCase();
            if (this.state.document.type.includes('pdf') || this.state.document.type.includes('image')) {
                this.typeFile = 1
            } else {
                this.typeFile = 2
            }
            this.fileUrl = `web/content/${document[0].attachment_id[0] || ''}`;
            // Set other document details
            this.state.document.name = document[0].name;
            this.state.document.project_id = document[0].project_id[1];
            this.state.document.file_size = formatBytes(document[0].file_size);
            this.state.document.create_uid = document[0].create_uid[1];
            this.state.document.tag_ids = tag_ids;
    
            // Use jQuery to add class to element
            $('.document_inspector').addClass('active');
        } catch (error) {
            console.error('Error fetching document details:', error);
            // Handle error as needed
        }
    }

    closeDetailDoc(){
        $('.document_inspector').removeClass('active')
    }

    async FindTagsDoc(tagId){
        const tags = await this.orm.searchRead('tags.ansv.document',[['id', 'in', tagId]],['name'])
        return tags
    }
    ontChangeFolderName(ev){
        $('#folderAdd').css('border-color', '');
    }

    async onInputChange(ev) {
        $('#project-select').css('border-color', '');
        // console.log(ev.target.id);
        switch (ev.target.id) {
            case 'project-select':
                var query = ev.target.value;
                // console.log(query)
                this.state.projects = await this.orm.searchRead('project.ansv', [
                    ['project_name', 'ilike', query]
                ], ["id", "project_name"])
                break;
            case 'owner-select':
                var query = ev.target.value;
                // console.log(query)
                this.state.owners = await this.orm.searchRead('res.users', [
                    ['name', 'ilike', query]
                ], ["id", "name"])
                break;
        }

    }

    //Downlaod File
    async downloadFile() {
        try {
            // var recordId = $('#doc_id').val();
            var recordId = this.state.docId
            console.log(recordId);
            const record = await this.orm.searchRead('ansv.file', [
                ['id', '=', recordId]
            ], ['name', 'attachment_data']);
            if (record.length > 0) {
                const {
                    name,
                    attachment_data
                } = record[0];
                const blob = this.base64ToBlob(attachment_data, 'application/octet-stream');
                const url = URL.createObjectURL(blob);
                const a = document.createElement('a');
                a.href = url;
                a.download = name;
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
                URL.revokeObjectURL(url);
            }
        } catch (error) {
            console.error('Error downloading file:', error);
        }
    }

    base64ToBlob(base64, mime) {
        const byteCharacters = atob(base64);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        return new Blob([byteArray], {
            type: mime
        });
    }
    //Downlaod File this is form base64 encoded 

    //Replace File
    async ReplaceFile() {
        console.log("Replace File Trigger");
        document.getElementsByClassName("o_inspector_replace_input")[0].click();
    }

    //Replace File
    handleFileReplace(event) {
        const recordId = this.state.docId;
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = (e) => {
                const fileContent = e.target.result.split(',')[1];
                // Strip the base64 prefix
                this._replaceFile(recordId, file.name, fileContent);
            };
            reader.readAsDataURL(file);
        }
    }
    async createFolder(){
                // Validate form inputs
        try {
            var namefolder = $('#folderAdd').val()
            if(namefolder){
                const result = await this.orm.create("ansv.file", [{
                    attachment_data: '',
                    name: namefolder,
                    is_folder: true,
                    project_id: this.project_id,
                    parent_id: this.parent_id,
                }, ])
                this.refreshModal();

                this.reloadKanbanView();
                // console.log(result);
                await this.fetchTreeData();
                $('#addFolderModal').modal('hide');
            }else{
                if(!namefolder){
                    $('#folderAdd').css({
                        borderColor: '#e53c3c61'
                    })
                }
            }
        } catch (error) {
            console.log(error);
        }
    }
    refreshModal(){
        $('#folderAdd').val('');
    }

    UploadFile() {
        this.getEnvValue()
        console.log("Upload New File...");
        document.getElementById("file").click();
    }

    handleFileChange(event) {
        const file = event.target.files[0];
        console.log("Selected file:", file);
        // Handle the file upload logic here
        if (file) {
            const reader = new FileReader();
            reader.onload = (e) => {
                const fileContent = e.target.result.split(',')[1]; // Strip the base64 prefix
                this._uploadFile(file.name, fileContent);
            };
            reader.readAsDataURL(file);
        }
    }

    async _uploadFile(filename, fileContent) {
        try {
            // if (!this.project_id){
            //     console.log("project enviroment undefined");
            // }
            const result = await this.orm.create("ansv.file", [{
                attachment_data: fileContent,
                name: filename,
                project_id: this.project_id,
                parent_id: this.parent_id,
            }, ])
            console.log("File uploaded successfully:", result);
            // this.saveFileToFileSystem(result);
            this.reloadKanbanView();
        } catch (error) {
            console.error("Failed to upload file:", error);
        }
    }

    async _replaceFile(recordId, filename, fileContent) {
        try {
            console.log(typeof (recordId));
            await this.orm.write("ansv.file", [parseInt(recordId)], {
                attachment_data: fileContent,
                name: filename,
            })
            console.log("Replace SuccessFull");
            this.reloadKanbanView();
        } catch (error) {
            console.error("Failed to replace file:", error);
        }
    }

    //Delete Kanban
    async deleteDoc() {
        const recordId = this.state.docId;
        await this.orm.unlink("ansv.file", [parseInt(recordId)])
        //refresh lại giá trị
        await this.fetchTreeData();
        this.reloadKanbanView()
    }

    // async saveFileToFileSystem(documentId) {
    //     try {
    //         rpc.query({
    //             model: 'ansv.file',
    //             method: 'save_file_to_filesystem',
    //             args: [documentId],
    //         }).then(function (result) {
    //             console.log(result);
    //         });
    //     } catch (error) {
    //         console.error('Failed to save file:', error);
    //     }
    // }

    reloadKanbanView() {
        //if the model add new data then reload
        this.model.load(this.model.loadParams)
            .then(() => {
                console.log("Reload OKE")
            })
            .catch((error) => {
                console.error("Error reloading the Kanban view:", error);
            });
    }
}
DocumentsViewController.components = {
    ...KanbanController.components,
    TreeNode,
};
DocumentsViewController.template = "owl.DocumentKanbanSideRecord"

//Edit your own record through Renderer
export class DocumentKanbanRecord extends KanbanRecord {
    setup() {
        super.setup();
    }
}

export class DocumentKanbanRenderer extends KanbanRenderer {
    setup() {
        super.setup()
        this.orm = useService("orm")
        // console.log("Hello from Tu Hoc vu");
        this.project_id = sharedValue.default.default_project_id;
        this.parent_id = sharedValue.default.default_parent_id;
        this.model = sharedValue.model;
    }



    onDragOver(event) {
        dropElement = event.target.closest('.folder-document');
        if (dropElement && draggedElement) {
            var dataId = $(dropElement).data('id');
            $('div[data-id="' + dataId + '"] button.ffolder').addClass("active");
        }else{
            $('.ffolder').removeClass("active");
        }

        event.preventDefault();
        // console.log(event);   
        // console.log(draggedElement);    
        if (!draggedElement) {
            $('.drop-zone').addClass("dragover");
        }
    }

    onDragLeave(event) {
        event.preventDefault();
        $('.drop-zone').removeClass("dragover");
    }

    onDrop(event) {
        if(dropElement){
            var dataId = $(dropElement).data('id');
            $('.ffolder').removeClass("active");
            var selectId = $(draggedElement).find('.o_documents_attachment').data('id');
            console.log(selectId);
            console.log(dataId);
            this._pullToFolder(selectId,dataId)
        }

        event.preventDefault();
        $('.drop-zone').removeClass("dragover");
        var files = event.dataTransfer.files;
        this.render();
        // Process the files here
        console.log(event.dataTransfer.files.length);
        if (files) {
            for (let i = 0; i < files.length; i++) {
                const reader = new FileReader();
                reader.onload = (e) => {
                    const fileContent = e.target.result.split(',')[1]; // Strip the base64 prefix
                    this._uploadFile(files[i].name, fileContent);
                };
                reader.readAsDataURL(files[i]);
            }
        }
    }

    async _pullToFolder(recID,folderID){
        try {
            await this.orm.write("ansv.file", [parseInt(recID)], {
                parent_id: folderID,
            })
            console.log("Pull to folder SuccessFull");
            this.reloadKanbanView();
        } catch (error) {
            console.error("Failed to replace file:", error);
        }
    }

    async _uploadFile(filename, fileContent) {
        try {
            // if (this.project_id == undefined){
            //     this.project_id = null
            // }
            this.project_id = this.env.searchModel.globalContext.default_project_id;
            this.parent_id = this.env.searchModel.globalContext.default_parent_id;
            const result = await this.orm.create("ansv.file", [{
                attachment_data: fileContent,
                name: filename,
                project_id: this.project_id,
                parent_id: this.parent_id,
            }, ])
            console.log("File uploaded successfully:", result);
            // this.saveFileToFileSystem(result);
            this.reloadKanbanView();
        } catch (error) {
            console.error("Failed to upload file:", error);
        }
    }

    reloadKanbanView() {
        //if the model add new data then reload
        this.model.load(this.model.loadParams)
            .then(() => {
                // Create the button element
                console.log("reload oke");
            })
            .catch((error) => {
                console.error("Error reloading the Kanban view:", error);
            });
    }
}

DocumentKanbanRenderer.components = {
    ...DocumentKanbanRenderer.components,
    KanbanRecord: DocumentKanbanRecord,
}

DocumentKanbanRenderer.template = "owl.DocumentKanbanRederer"

export const DocumentKanbanview = {
    ...kanbanView,
    Renderer: DocumentKanbanRenderer,
    Controller: DocumentsViewController,
    buttonTemplate: 'owl.DocumentKanbanView.Buttons',
    setup() {
        super.setup();
        this.selectDocumentId = '123123';
    }
}

registry.category("views").add("document_kanban", DocumentKanbanview)