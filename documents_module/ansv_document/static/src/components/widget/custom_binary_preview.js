/** @odoo-module **/

import { registry } from "@web/core/registry";
import { Component } from "@odoo/owl";

class CustomBinaryPreview extends Component {
    setup() {
        console.log(this.props.record.data);
        this.fileName = this.props.record.data.display_name;
        this.type = this.props.record.data.mimetype;
        console.log(typeof(this.type));
        this.fileUrl = this.props.value ? `web/content/${ this.props.record.data.attachment_id[0]}` : false;
    }

    async onFileChange(event) {
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = (e) => {
            const base64 = e.target.result.split(',')[1];
            this.props.updateData({
                attachment_data: base64,
                name: file.name,
            });
        };
        reader.readAsDataURL(file);
    }
}

CustomBinaryPreview.template = "ansv_document.CustomBinaryPreview";

registry.category("fields").add("custom_binary_preview", CustomBinaryPreview);