$(document).mouseup(function(e)
{
    var container = $("#dropdown-project");
    var container2 = $("#dropdown-owner");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) || !container2.is(e.target))
    {
        container.hide();
        container2.hide();
    }
});