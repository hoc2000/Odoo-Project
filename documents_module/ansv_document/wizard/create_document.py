from odoo import api, fields, models
import datetime


class SolutionForTicket(models.TransientModel):
    _name = "add.project.wizard"
    _description = "Before Document Created"

    # @api.model
    # def default_get(self, fields_list):
    #     res = super(SolutionForTicket, self).default_get(fields_list)
    #     print("Default get", res)
    #     res['cancel_date'] = datetime.date.today()
    #     return res

    project_id = fields.Many2one('project.ansv', string="Your Project")
    tag_ids = fields.Many2many('tags.ansv.document', 'rel_file_file', 'tag_id', 'file_id', 'tag')

    def action_cancel(self):
        print("Modal wizard successfully click...........!")
        # Take the cuernet record value change it with the wizard
        self.env['mockdesk.ticket'].browse(self._context.get("active_ids")).update({'solution': self.solution})
        return True
