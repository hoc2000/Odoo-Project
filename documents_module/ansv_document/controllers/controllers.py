from odoo import http
from odoo.http import request


class DocumentController(http.Controller):

    @http.route('/documents/inform', type='json', auth='user')
    def document_banner(self):
        return {'html': """
            <div class="banner-route-document">
                <div class="content-banner">
                    <h1 class = "text-white">THV is da best</h1>
                    <p>Ya-hoo</p>
                </div>
            </div>
        """}
