/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./custom_module/**/*.{html,js,xml}"], // Make sure to include your custom addons' paths here as well
  theme: {
    extend: {},
  },
  plugins: [],

  blocklist: ['container', 'collapse'], // Add classes to blocklist to avoid generating them
};